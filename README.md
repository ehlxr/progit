# About
This project aims to generate html pages for *Pro Git* book from [*Book Source*](https://github.com/progit/progit) using node.js.

The generated HTML pages can be hosted on the [GitHub](http://github.com) via [Github Pages](https://pages.github.com). 

## Demo
 - [demo](https://ehlxr.github.io/progit).

## Screenshots

 - [Desktop](assets/img/preview-desktop.png?raw=true)
 - [Tablet](assets/img/preview-tablet.png?raw=true)
 - [Mobile](assets/img/preview-mobile.png?raw=true)


 ![Mobile](assets/img/preview-mobile.png?raw=true)
